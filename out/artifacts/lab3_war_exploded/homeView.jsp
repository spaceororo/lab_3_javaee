<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>Home Page</title>
</head>
<body>

<jsp:include page="_header.jsp"></jsp:include>
<h3>Home Page</h3>
<br>
<b>Why "Space Tour" </b>
<ul>
    <li>We offer tours to any corner of the universe. Do you dream of visiting the cold planet Pluto or the hot Venus?
        Not a problem!
    </li>
    <li>For those who love the burning feelings, you can get as close to the Sun as possible!</li>
    <li> A huge base of ships for every taste. Going on vacation with your family? We have great offers for families
        with small children.
    </li>
    <li> We care about customers 24 \ 7. If you have any questions or difficulties traveling, we are always there. Every
        customer is important to us!
    </li>
    <li> The most developed network of travel agency offices in Ukraine. Wherever you are, we are here!</li>
    <li> Travel insurance at the request of the client.</li>
    <li> You saw the tour of your dreams, but there is no way to pay all at once? Not a problem: with us you can always
        buy an installment tour. Relax now - and pay later!
    </li>
</ul>

</body>
</html>