<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="ISO-8859-1">
    <title>Edit tour</title>
</head>
<body>

<h1>Edit Tour</h1>

<form action="EditTour" method="POST">
    <p>Code: ${tour.idTour}</p>
    <input type="hidden" name="code" value="${tour.idTour}">
    <p>Name</p>
    <input name="name" value="${tour.nameTour}" required oninvalid="this.setCustomValidity('Fill this field!')"
           oninput="setCustomValidity('')"/> <br>
    <p>Description</p>
    <p><textarea rows="10" cols="40" name="desc" required oninvalid="this.setCustomValidity('Fill this field!')"
                 oninput="setCustomValidity('')">${tour.descTour}</textarea></p> <br>
    <input type="submit" value="Save"> <br>
</form>
</body>
</html>