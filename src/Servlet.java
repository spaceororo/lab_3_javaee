import java.io.IOException;
        import java.io.PrintWriter;
        import javax.servlet.RequestDispatcher;
        import javax.servlet.ServletContext;
        import javax.servlet.ServletException;
        import javax.servlet.annotation.WebServlet;
        import javax.servlet.http.HttpServlet;
        import javax.servlet.http.HttpServletRequest;
        import javax.servlet.http.HttpServletResponse;

@WebServlet("/Servlet")
public class Servlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter writer = response.getWriter();
        try {
            writer.print("Hello, WebWorld!!!");
            writer.print("<table>");
            writer.print("<tr><td>Name</td><td>Liu I.V.</td></tr>");
            writer.print("<tr><td>Group</td><td>KN-37e</td></tr>");
            writer.print("<tr><td>Number</td><td>I don't know</td></tr>");
            writer.print("</table>");
        }finally {
            writer.close();
        }
    }
}