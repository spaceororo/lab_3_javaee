package model;

import java.io.Serializable;
import java.util.Objects;

public class Tour implements Serializable {
    private int idTour;
    private String nameTour;
    private String descTour;

    public Tour(int idTour, String nameTour, String descTour) {
        this.idTour = idTour;
        this.nameTour = nameTour;
        this.descTour = descTour;
    }

    public Tour(String nameTour, String descTour) {
        this.nameTour = nameTour;
        this.descTour = descTour;
    }

    public Tour() {
    }

    public int getIdTour() {
        return idTour;
    }

    public void setIdTour(int idTour) {
        this.idTour = idTour;
    }

    public String getNameTour() {
        return nameTour;
    }

    public void setNameTour(String nameTour) {
        this.nameTour = nameTour;
    }

    public String getDescTour() {
        return descTour;
    }

    public void setDescTour(String descTour) {
        this.descTour = descTour;
    }


    public boolean equals(Tour o) {
        boolean TF = true;
        if (this.nameTour == o.nameTour)
            TF = false;
        return TF;
    }

    @Override
    public int hashCode() {
        return Objects.hash(idTour, nameTour, descTour);
    }

    @Override
    public String toString() {
        return "Tour{" +
                "idTour=" + idTour +
                ", nameTour='" + nameTour + '\'' +
                ", descTour='" + descTour + '\'' +
                '}';
    }
}
