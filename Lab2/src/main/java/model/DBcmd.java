package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedHashSet;

public class DBcmd {
    public static LinkedHashSet<Tour> queryTour(Connection conn) throws SQLException {
        String sql = "Select * from tours";
        PreparedStatement pstm = conn.prepareStatement(sql);
        ResultSet rs = pstm.executeQuery();
        LinkedHashSet tours = new LinkedHashSet();
        while (rs.next()) {
            int id_tour = rs.getInt("id_tour");
            String name_tour = rs.getString("name_tour");
            String desc_tour = rs.getString("desc_tour");
            Tour tour = new Tour(id_tour, name_tour, desc_tour);
            tours.add(tour);
        }
        pstm.close();
        return tours;
    }

    public static LinkedHashSet<Tour> queryFindTourById(Connection conn, String id_tour_search) throws SQLException {
        String sql = "Select * from tours where id_tour=?";

        PreparedStatement pstm = conn.prepareStatement(sql);
        pstm.setString(1, id_tour_search);

        ResultSet rs = pstm.executeQuery();
        LinkedHashSet tours = new LinkedHashSet();
        while (rs.next()) {
            int id_tour = rs.getInt("id_tour");
            String name_tour = rs.getString("name_tour");
            String desc_tour = rs.getString("desc_tour");
            Tour tour = new Tour(id_tour, name_tour, desc_tour);
            tours.add(tour);
        }
        pstm.close();
        return tours;
    }

    public static LinkedHashSet queryFindTourByName(Connection conn, String name_tour_search) throws SQLException {
        String sql = "Select * from tours where name_tour=?";

        PreparedStatement pstm = conn.prepareStatement(sql);
        pstm.setString(1, name_tour_search);
        ResultSet rs = pstm.executeQuery();
        LinkedHashSet tours = new LinkedHashSet();
        while (rs.next()) {
            int id_tour = rs.getInt("id_tour");
            String name_tour = rs.getString("name_tour");
            String desc_tour = rs.getString("desc_tour");
            Tour tour = new Tour(id_tour, name_tour, desc_tour);
            tours.add(tour);
        }
        return tours;
    }

    public static boolean queryUpdateTour(Connection conn, Tour tour) throws SQLException {
        String sql = "Update tours set name_tour =?, desc_tour=? where id_tour=? ";
        PreparedStatement pstm = conn.prepareStatement(sql);
        pstm.setString(1, tour.getNameTour());
        pstm.setString(2, tour.getDescTour());
        pstm.setInt(3, tour.getIdTour());
        pstm.executeUpdate();
        return true;
    }

    public static boolean queryInsertTour(Connection conn, Tour tour) throws SQLException {
        String sql = "Insert into tours(name_tour, desc_tour) values (?,?)";
        PreparedStatement pstm = conn.prepareStatement(sql);
        pstm.setString(1, tour.getNameTour());
        pstm.setString(2, tour.getDescTour());
        pstm.executeUpdate();
        return true;
    }

    public static boolean queryDeleteTour(Connection conn, String code) throws SQLException {
        String sql = "Delete From tours where id_tour= ?";
        PreparedStatement pstm = conn.prepareStatement(sql);
        pstm.setString(1, code);
        pstm.executeUpdate();
        return true;
    }
}
