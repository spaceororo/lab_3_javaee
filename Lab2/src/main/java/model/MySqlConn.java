package model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class MySqlConn {
    public static Connection getMySqlConn()
            throws ClassNotFoundException, SQLException {
        String hostName = "localhost";
        String dbName = "spacetour";
        String userName = "myuser";
        String password = "12345";
        return getMySqlConn(hostName, dbName, userName, password);
    }

    public static Connection getMySqlConn(String hostName, String dbName,
                                          String userName, String password) throws SQLException,
            ClassNotFoundException {
        Class.forName("com.mysql.cj.jdbc.Driver");
        String connectionURL = "jdbc:mysql://" + hostName + ":3306/" + dbName;
        Connection conn = DriverManager.getConnection(connectionURL, userName,
                password);
        return conn;
    }

    public static void main(String[] args) {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/spacetour", "root",
                    "12345");
            System.out.println("Connected");
            Statement statement = conn.createStatement();
            statement.executeUpdate("insert into tours (name_tour,desc_tour) values ('from Java','from Java')");
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
/*
public class MySqlConn {
    public static void main(String[] args) {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection conn=DriverManager.getConnection("jdbc:mysql://localhost:3306/spacetour", "root",
                    "12345");
            System.out.println("Connected");
            Statement statement = conn.createStatement();
            statement.executeUpdate("insert into tours (name_tour,desc_tour) values ('from Java','from Java')");
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
*/