package servlets;

import model.User;
import model.Users;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet("/Login")
public class LoginServlet extends HttpServlet {
    public LoginServlet() {
        super();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String errorString = "";
        boolean hasError = false;
        String adminU = "admin";
        String adminP = "admin";
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        System.out.println(request.getParameter("username"));
        boolean TF = true;

        if (Users.chek(request.getParameter("username"), request.getParameter("password"))) {
            System.out.println("Redirect to Print Tour");
            request.setAttribute("TF", TF);
            RequestDispatcher dispatcher = request.getServletContext().getRequestDispatcher("/Home");
            dispatcher.forward(request, response);
        } else {
            errorString = "Usernaame or password invalid";
            request.setAttribute("errorString", errorString);

            hasError = true;
        }

        if (hasError) {
            request.setAttribute("errorString", errorString);
            RequestDispatcher dispatcher = this.getServletContext().getRequestDispatcher("/Login.jsp");
            dispatcher.forward(request, response);
        }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getServletContext().getRequestDispatcher("/Login.jsp");
        dispatcher.forward(request, response);
    }

    @Override
    public void destroy() {
        super.destroy();
    }
}
