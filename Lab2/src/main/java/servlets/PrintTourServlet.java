package servlets;


import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.*;

@WebServlet("/PrintTour")
public class PrintTourServlet extends HttpServlet {
    public PrintTourServlet() {
        super();
    }

    private static final long serialVersionUID = 1L;

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LinkedHashSet<Tour> t = new LinkedHashSet();
        Connection conn = null;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            conn = MySqlConn.getMySqlConn("localhost", "spacetour", "myuser", "12345");
            System.out.println("Connected");
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        LinkedHashSet<Tour> listTour = null;
        try {
            listTour = DBcmd.queryTour(conn);
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        request.setAttribute("tourList", listTour);
        System.out.println("tourList in PrintTout..." + request.getAttribute("tourList"));
        RequestDispatcher rd = getServletContext().getRequestDispatcher("/PrintTour.jsp");
        rd.forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }

    @Override
    public void destroy() {
        super.destroy();
    }
}
