package servlets;

import model.CollectionTour;
import model.Tour;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.beans.XMLDecoder;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.LinkedHashSet;

@WebServlet("/Open")
public class OpenServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LinkedHashSet<Tour> line = null;
        try (XMLDecoder xmlDecoder = new XMLDecoder(new FileInputStream("d:\\ser.xml"))) {
            line = (LinkedHashSet<Tour>) xmlDecoder.readObject();
            request.setAttribute("tourList", line);
            RequestDispatcher dispatcher = request.getServletContext().getRequestDispatcher("/PrintTour.jsp");
            dispatcher.forward(request, response);
        } catch (IOException e) {
            e.printStackTrace();
        }

        for (Tour tour : CollectionTour.getList()) {
            System.out.println(tour);
        }

    }
}
