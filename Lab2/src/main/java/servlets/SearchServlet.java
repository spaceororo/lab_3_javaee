package servlets;

import model.DBcmd;
import model.MySqlConn;
import model.Tour;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sound.sampled.Line;
import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedHashSet;

@WebServlet("/Search")
public class SearchServlet extends HttpServlet {
    public SearchServlet() {
        super();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Connection conn = null;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            conn = MySqlConn.getMySqlConn("localhost", "spacetour", "myuser", "12345");
            System.out.println("Connected in Search...");
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        try {
            LinkedHashSet<Tour> tourF = DBcmd.queryFindTourById(conn, request.getParameter("search"));
            request.setAttribute("tourList", tourF);
            RequestDispatcher rd = getServletContext().getRequestDispatcher("/PrintTour.jsp");
            rd.forward(request, response);
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        response.sendRedirect("PrintTour");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.sendRedirect("PrintTour");
    }

    @Override
    public void destroy() {
        super.destroy();
    }
}
