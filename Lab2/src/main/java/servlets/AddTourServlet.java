package servlets;

import model.CollectionTour;
import model.DBcmd;
import model.MySqlConn;
import model.Tour;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

@WebServlet("/AddTour")
public class AddTourServlet extends HttpServlet {
    public AddTourServlet() {
        super();
    }

    public static int idTour = CollectionTour.getList().getidlasttour();

    // public int idTour = 4;
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String name = (String) request.getParameter("name");
        String desc = (String) request.getParameter("desc");
        //int idTour = Integer.parseInt(request.getParameter("code"));
        Connection conn = null;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            conn = MySqlConn.getMySqlConn("localhost", "spacetour", "myuser", "12345");
            System.out.println("Connected in Add...");
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        idTour += 1;
        System.out.println("idTour    ------>" + idTour);
        String errorText = null;
        Tour addTour = new Tour(name, desc);

        try {
            if (DBcmd.queryInsertTour(conn, addTour)) {
                addTour = null;
                conn.close();
                response.sendRedirect("PrintTour");
            } else {
                errorText = "Data has mistakes!!! Try again...";
                System.out.println("Data has mistakes!!! Try again");
                request.setAttribute("tour", addTour);
                request.setAttribute("error", errorText);
                RequestDispatcher dispatcher = request.getServletContext().getRequestDispatcher("/AddTour.jsp");
                dispatcher.forward(request, response);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        System.out.println("tourList in AddTour finish..." + request.getAttribute("tourList"));
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("tourList in AddTout start..." + request.getAttribute(CollectionTour.getList().toString()));
        RequestDispatcher dispatcher = request.getServletContext().getRequestDispatcher("/AddTour.jsp");
        dispatcher.forward(request, response);
    }

    @Override
    public void destroy() {
        super.destroy();
    }
}
