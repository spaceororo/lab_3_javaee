package servlets;

import model.CollectionTour;
import model.DBcmd;
import model.MySqlConn;
import model.Tour;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Iterator;

@WebServlet("/EditTour")
public class EditTourServlet extends HttpServlet {
    public EditTourServlet() {
        super();
    }


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String name = (String) request.getParameter("name");
        String desc = (String) request.getParameter("desc");
        int id = Integer.parseInt(request.getParameter("code"));
        Tour newTour = new Tour(id, name, desc);
        /*
        if (CollectionTour.addTour(newTour)) {
            newTour = null;
            response.sendRedirect("PrintTour");
        } else {
            request.setAttribute("tour", newTour);
            RequestDispatcher dispatcher = request.getServletContext().getRequestDispatcher("/EditTour.jsp");
            dispatcher.forward(request, response);
        }*/
        Connection conn = null;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            conn = MySqlConn.getMySqlConn("localhost", "spacetour", "myuser", "12345");
            System.out.println("Connected in Add...");
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        try {
            if (DBcmd.queryUpdateTour(conn, newTour)) {
                newTour = null;
                conn.close();
                response.sendRedirect("PrintTour");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Tour tour = new Tour(Integer.parseInt(request.getParameter("idTour")), (String) request.getParameter("nameTour"), (String) request.getParameter("descTour"));
        request.setAttribute("tour", tour);
        //CollectionTour.delTour(tour.getIdTour());
        RequestDispatcher dispatcher = request.getServletContext().getRequestDispatcher("/EditTour.jsp");
        dispatcher.forward(request, response);
    }

    @Override
    public void destroy() {
        super.destroy();
    }
}
