import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/AboutAuthor")
public class About extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        PrintWriter writer=resp.getWriter();
        String name = req.getParameter("name");
        String password = req.getParameter("password");
        if(req.getParameter("login")!=null)
        {

            if((name.equals("admin"))&&(password.equals("admin"))) {
                resp.sendRedirect("/main.html");
            }else
                resp.sendRedirect("/");
        }
        if(req.getParameter("ButAbout")!=null)
        {
            resp.sendRedirect("/about.html");
        }
        if(req.getParameter("MyList")!=null)
        {
            try {
                writer.write("<html>" +
                        "<head>\n" +
                        "    <meta charset=\"UTF-8\">\n" +
                        "    <title>My list</title>\n" +
                        "<link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css\" integrity=\"sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T\" crossorigin=\"anonymous\">" +
                        "</head>"+
                        "<body>"+
                        "<table class=\"table table-hover\">\n" +
                        "    <thead>\n" +
                        "        <tr>\n" +
                        "            <th>Number</th>\n" +
                        "            <th>Name</th>\n" +
                        "            <th>First name</th>\n" +
                        "            <th>E-mail</th>\n" +
                        "        </tr>\n" +
                        "    </thead>\n" +
                        "    <tbody>\n" +
                         "        <tr>\n" +
                        "            <td>1</td>\n" +
                        "            <td>Ivan</td>\n" +
                        "            <td>Ivanov</td>\n" +
                        "            <td>ivan@mail.ru</td>\n" +
                        "        </tr>\n" +
                        "        <tr>\n" +
                        "            <td>2</td>\n" +
                        "            <td>Kot</td>\n" +
                        "            <td>Kotov</td>\n" +
                        "            <td>petr@mail.ru</td>\n" +
                        "        </tr>\n" +
                        "        <tr>\n" +
                        "            <td>3</td>\n" +
                        "            <td>Somebody</td>\n" +
                        "            <td>Golov</td>\n" +
                        "            <td>gol@mail.ru</td>\n" +
                        "        </tr>\n" +
                        "        <tr>\n" +
                        "            <td>4</td>\n" +
                        "            <td>Shut</td>\n" +
                        "            <td>Shutov</td>\n" +
                        "            <td>sh@mail.ru</td>\n" +
                        "        </tr>\n" +
                        "        <tr>\n" +
                        "            <td>5</td>\n" +
                        "            <td>Vivo</td>\n" +
                        "            <td>Vivov</td>\n" +
                        "            <td>vi@mail.ru</td>\n" +
                        "        </tr>\n" +
                        "        <tr>\n" +
                        "            <td>6</td>\n" +
                        "            <td>Kit</td>\n" +
                        "            <td>kitov</td>\n" +
                        "            <td>kit@mail.ru</td>\n" +
                        "        </tr>\n" +
                        "        <tr>\n" +
                        "            <td>7</td>\n" +
                        "            <td>Alex</td>\n" +
                        "            <td>Oly</td>\n" +
                        "            <td>o@mail.ru</td>\n" +
                        "        </tr>\n" +
                        "        <tr>\n" +
                        "            <td>8</td>\n" +
                        "            <td>Foo</td>\n" +
                        "            <td>Food</td>\n" +
                        "            <td>f@mail.ru</td>\n" +
                        "        </tr>\n" +
                        "        <tr>\n" +
                        "            <td>9</td>\n" +
                        "            <td>Tik</td>\n" +
                        "            <td>Tok</td>\n" +
                        "            <td>Took@mail.ru</td>\n" +
                        "        </tr>\n" +
                        "    </tbody>\n" +
                        "</table>" +
                        "</body>\n" +
                        "</html>");
            }
            finally {
                writer.close();
            }
        }
    }
}